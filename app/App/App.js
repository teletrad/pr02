
import React, { Component } from 'react'
import { Router, Route, browserHistory } from 'react-router'
import { Redirect } from 'react-router-dom'
import { ConnectedRouter, routerReducer, routerMiddleware, push, syncHistoryWithStore } from 'react-router-redux'
import createHistory from 'history/createBrowserHistory'

import { Provider, connect } from 'react-redux'
import { createStore, compose, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'

import Login from '../components/Login'
import Home from '../components/Home'
import Register from '../components/Register'
import EnsureLoginContainer from '../containers/EnsureLoginContainer'



import appReducer from '../reducers/appReducer'

// Create a history of your choosing (we're using a browser history in this case)
const history = createHistory()

// Build the middleware for intercepting and dispatching navigation actions
const middleware = routerMiddleware(history)

const middlewares = [...middleware, thunk]

const enhancer = compose(applyMiddleware(...middlewares))

// Add the reducer to your store on the `router` key
// Also apply our middleware for navigating
const store = createStore(appReducer, enhancer)


// Now you can dispatch navigation actions from anywhere!
// store.dispatch(push('/foo'))

const Protected = () => <h3>Protected</h3>

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route {...rest} render={props => (
    // fakeAuth.isAuthenticated ? (
      store.logged ? (
      <Component {...props}/>
    ) : (
      <Redirect to={{
        pathname: '/login',
        state: { from: props.location }
      }}/>
    )
  )}/>
)

export default class App extends Component {

  // TODO: add go back
  notFound = () => <div><h3>404 Not Found</h3></div>

  render() {
    return (
      <Provider store={store}>
      { /* ConnectedRouter will use the store from Provider automatically */ }
        <ConnectedRouter history={history}>
          <div>
            <Route exact path="/" component={Home}/>
            <Route path="/login" component={Login}/>
            <Route path="/register" component={Register}/>
            {/*<PrivateRoute path="/protected" component={Protected}/>
            <PrivateRoute path="/register" component={Register}/>*/}
            {/*<Route component={this.notFound} />*/}
          </div>
        </ConnectedRouter>
      </Provider>    
    )
  }
}

// <Route component={EnsureLoginContainer}>
//               <Route path="/register" component={Register} />
//             </Route>

// const mapStateToProps = (state) => ({
//   logged: state.loginReducer.logged
// })

// const mapDispatchToProps = (dispatch) => ({
//   dispatch
// })

// // export default App
// export default connect(mapStateToProps, mapDispatchToProps)(App);





