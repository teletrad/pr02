import React, { Component } from 'react'
import { connect } from 'react-redux'
import * as actions from '../reducers/actions'
import { push } from 'react-router-redux'

class Home extends Component {
  state = { 
    email: '',
    pass: '',
    pass2: '',
    userData: '',
    block: true,
    passOK: false,
    emailOK: false,
    userDataOK: false,
    showPassError: false
  }
 
  setEmail = (e) => {
    this.setState({
      email: e.target.value
    })
  }

 // TODO: add email validation
  validateEmail = () => {
    this.setState({
      emailOK: true
    })
  }

  setPass = (e) => {
    this.setState({
      pass: e.target.value
    })
  }

  setPass2 = (e) => {
    this.setState({
      pass2: e.target.value
    })
  }

  setUserData = (e) => {
    this.setState({
      userData: e.target.value
    })
  }

  setUserDataOK = (e) => {
    if (this.state.userData !== '') {
      this.setState({
        userDataOK: true
      })
    }
  }

  checkPass = () => {
    if (this.state.pass === this.state.pass2) {
      this.setState({
        passOK: true,
        showPassError: false
      })
    } else {
      this.setState({
        passOK: false,
        showPassError: true
      })
    }
  }  

  redirect = () => {

  // Invert control!
  // Return a function that accepts `dispatch` so we can dispatch later.
  // Thunk middleware knows how to turn thunk async actions into actions.

  return (dispatch) => {
    dispatch(push('/login'))
    // return fetchSecretSauce().then(
    //   sauce => dispatch(makeASandwich(forPerson, sauce)),
    //   error => dispatch(apologize('The Sandwich Shop', forPerson, error))
    // );
  };
}

  register = () => {
    this.props.dispatch(push('/login'))
    console.log(this.state.emailOK, this.state.passOK,this.state.userDataOK)
    if ( this.state.emailOK && this.state.passOK && this.state.userDataOK) {
      // this.props.setLoggedIn()
      // this.props.dispatch(push('/register'))
      // this.props.setUser({user: 'user'})
      // this.props.dispatch({
      //   type: 'CREATE_USER',
      //   user: {user: this.state.email, pass: this.state.pass, data: this.state.userData}
      // })
      // this.props.dispatch(this.redirect())
      this.props.dispatch(push('/login'))
    } else {
      this.setState({
        block: false
      })
    }
  }

  // TODO: Try use history.push('/register')
  login = () => {
    this.props.dispatch(push('/login'))
  }

  render() {
    return (
      <div>
        email: <input name="email" id="email" type="text" onChange={this.setEmail} onBlur={this.validateEmail} /> <br />
        pass: <input name="pass" id="pass" type="text" onChange={this.setPass} /> <br />
        confirm pass: <input name="pass2" id="pass2" type="text" onChange={this.setPass2} onBlur={this.checkPass} /> 
          <span style={{display: this.state.showPassError ? '' : 'none'}}>Passwords do not match</span> <br />
        content: <textarea name="userData" id="userData" cols="30" rows="10" onChange={this.setUserData} onBlur={this.setUserDataOK} ></textarea> <br />
        <button onClick={this.register}>Register</button>
        <button onClick={this.login}>Login</button>
        <div style={this.state.block ? {display: 'none'} : {color: 'red'}}>Complete all registration fields!</div>
      </div>
    );
  }
}


export default connect((state) => ({
  logged: state.loginReducer.logged
}), (dispatch) => ({
  dispatch,
  setLoggedIn: actions.LOGIN_ACTION_CREATOR,
  setUser: actions.CREATE_USER_ACTION_CREATOR
}))(Home);

