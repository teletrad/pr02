import React, { Component } from 'react'
import { connect } from 'react-redux'
import { push } from 'react-router-redux'

import * as actions from '../reducers/actions'

class Login extends Component {
  state = {  }

  register = () => {
    this.props.dispatch(push('/'))
  }

  render() {
    return (
      <div>
        username: <input type="text" /> <br />
        pass: <input type="text" /> <br />
        <button>Submit</button>
        <button onClick={this.register}>Register</button>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({

})

const mapDispatchToProps = (dispatch) => ({
  dispatch
})

export default connect(mapStateToProps, mapDispatchToProps)(Login);


