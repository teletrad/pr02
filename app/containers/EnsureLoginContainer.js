import React, { Component } from 'react'
import { connect } from 'react-redux'

class EnsureLoginContainer extends Component {
  state = {  }
  render() {
    return (
      <div>
        {this.props.isLoggedIn ? this.props.children : 'null'}
      </div>
    );
  }
}

// Grab a reference to the current URL. If this is a web app and you are
// using React Router, you can use `ownProps` to find the URL. Other
// platforms (Native) or routing libraries have similar ways to find
// the current position in the app.
function mapStateToProps(state, ownProps) {
  return {
    isLoggedIn: state.loginReducer.logged,
    currentURL: ownProps.location.pathname
  }
}

export default connect(mapStateToProps)(EnsureLoginContainer)


