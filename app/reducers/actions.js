export const LOGIN_ACTION_CREATOR = () => ({
  type: 'LOGIN'
})

export const LOGOUT_ACTION_CREATOR = () => ({
  type: 'LOGOUT'
})

export const CREATE_USER_ACTION_CREATOR = (v) => ({
  type: 'CREATE_USER',
  payload: v
})

export const FETCH_USER_ACTION_CREATOR = (v) => ({
  type: 'FETCH_USER',
  payload: v
})

