import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'

import loginReducer from './loginReducer'
import userReducer from './userReducer'

// Add the reducer to your store on the `router` key
const appReducer = combineReducers({
  loginReducer,
  userReducer,
  routing: routerReducer
})

export default appReducer