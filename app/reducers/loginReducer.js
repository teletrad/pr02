
import React, { Component } from 'react'
import { push } from 'react-router-redux'

const initialState = {
  logged: false
}

export default (state = initialState, action) => {
  switch (action.type) {
    case 'LOGIN':
    //  dispatch(push('/register'))
      return {
        ...state,
        logged: true
      }
    case 'LOGOUT':
      return {
        ...state,
        logged: false
      }  
    default:
      return state
  }
}
