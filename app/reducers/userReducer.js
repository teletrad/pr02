const initialState = {
  user: {}
}

export default (state = initialState, action) => {
  switch (action.type) {
    case 'CREATE_USER':
      // dispatch({type: 'LOGIN'})
      return state
    case 'FETCH_USER':
      return state
    default:
      return state
  }
}
