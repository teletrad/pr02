import React, { Component } from 'react'

export default class Root extends Component {
  constructor(props){
    super(props)
  }
  state = {  }
  render() {
    return (
      <div>
        {this.props.children}
      </div>
    );
  }
}