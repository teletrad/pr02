import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { AppContainer } from 'react-hot-loader'

import { Provider } from 'react-redux'
import { createStore, compose, applyMiddleware } from 'redux'
// import createHistory from 'history/createBrowserHistory'
// import { ConnectedRouter, routerReducer, routerMiddleware, push } from 'react-router-redux'

import appReducer from './app/reducers/appReducer'
// import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
// import injectTapEventPlugin from 'react-tap-event-plugin'


// injectTapEventPlugin()

import App from './app/App/App'

// const store = createStore(appReducer)

// <Provider store={store}>
// 				<Router history={newHistory}>
// 					<MuiThemeProvider>
// 	</MuiThemeProvider>
// 				</Router>
// 			</Provider>
const render = Component => {
	ReactDOM.render (		
		<AppContainer>
			<Component />
		</AppContainer>,
		document.getElementById('app')
	)
}

render(App)