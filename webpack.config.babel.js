const path = require('path');
// const HtmlWebpackPlugin = require('html-webpack-plugin');
import HtmlWebpackPlugin from 'html-webpack-plugin'
import webpack from 'webpack'

const HtmlWebpackPluginConfig = new HtmlWebpackPlugin({
  template: './index.html',
  filename: 'index.html',
  inject: 'body'
})

const JS_LOADER = {
	test:/\.(js|jsx)$/,
	loaders:[
		'babel-loader'
	],
	exclude:/node_modules/
}

const DEVELOPMENT = process.env.NODE_ENV === 'development'

const config = {
	devtool: DEVELOPMENT ? 'cheap-module-inline-source-map' : 'cheap-source-map',
  entry: [
		'react-hot-loader/patch',
		'webpack-dev-server/client?http://localhost:8085',
		'webpack/hot/only-dev-server',
		'./index.js'		
	],
  output: {
    path: path.resolve('dist'),
    filename: 'index_bundle.js'
  },
  module: {
    loaders: [
			JS_LOADER
    ]
  },

  devServer: {
		host: 'localhost',
		port: 8085,
		historyApiFallback: true,
		hot: true
	},

	plugins: [
		new webpack.HotModuleReplacementPlugin(), // enable-uje hmr globalno
		new webpack.NamedModulesPlugin(),
		new webpack.NoEmitOnErrorsPlugin(),
		HtmlWebpackPluginConfig
	]
}

module.exports = config

